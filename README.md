
Li2++
-------

`Li2++` is a C++ library for fast multi-precision numerical evaluation of dilogarithms

The approach is based on the one described in 't Hooft and Veltman [NPB153:365 (1979)],
and also documented in Hameren, Vollinga and Weinzierl [Eur.Phys.J.C41:361-375 (2005)]

For the most part, the implementation is taken, with some modifications
from the (unpublished) C++ library [BlackHat](https://arxiv.org/abs/0803.4180)

The original implementation is by Daniel Maitre and David Kosower.


## Installation

`Li2++` uses [meson](https://mesonbuild.com/) build system.
If meson is not already available in the system, it can be installed with

```
pip3 install --user meson
```

In short, to build and install `Li2++` do this:

```
mkdir build
cd build
meson .. -D prefix=$HOME/local
ninja
ninja install
```

This will install `Li2++` to `$HOME/local`.

To run tests, execute

```
meson test
```

in the build directory.


`Li2++` is a library which can be used in other C++ projects in the standard manner.
The interface header is *Li2++/Li2++.h*.
The library is shipped with a `pkg-config` file *Li2++.pc*, so the recommended way 
is to use the `pkg-config` facilities of your favorite build system.

`Li2++` supports evaluations of dilogarithms with quadruple and octuple precision
using the `dd_real` and `qd_real` types of the [qd](http://crd-legacy.lbl.gov/~dhbailey/mpdist/)
library.

To enable this, one needs to configure with

```
meson .. -D prefix=$HOME/local -D high-precision=true
```

[NPB153:365 (1979)]: https://doi.org/10.1016/0550-3213(79)90605-9
[Eur.Phys.J.C41:361-375 (2005)]: https://doi.org/10.1140/epjc/s2005-02229-6
