#include <iostream>
#include <iomanip>
#include <complex>

#include "Li2++.h"

int main() {
    using namespace Li2pp;
    using std::complex;

    std::cout << std::scientific << std::setprecision(17);

    std::cout << ReLi2(0.21) << std::endl;
    std::cout << Li2(complex<double>(0.3,-0.6)) << std::endl;
    std::cout << Cl2(0.3) << std::endl;

#ifdef Li2pp_QD_ENABLED 
    std::cout  << std::setprecision(32);
    std::cout << ReLi2(RHP("0.21")) << std::endl;

    std::cout  << std::setprecision(64);
    std::cout << ReLi2(RVHP("0.21")) << std::endl;
#endif // Li2pp_QD_ENABLED


    return 0;
}
