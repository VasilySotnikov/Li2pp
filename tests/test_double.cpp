#include "catch.hpp"

#include "Li2++.h"
#include "utilities.hpp"

#include <iostream>
#include <iomanip>

using namespace Li2pp;

TEST_CASE("Double precision real", "[R]") {
    using T = R;
    double tol = 14;

    {
        T x = 0.1298900108239589190;
        T target = 0.134370773311533513;
        CHECK(check(ReLi2(x),target,tol));
    }

    {
        T x = 2.59207254461462806;
        T target = 2.4050448711440321;
        CHECK(check(ReLi2(x), target, tol));
    }

    {
        T x = -4.66083787099030172;
        T target = -2.6254688699611638;
        CHECK(check(ReLi2(x), target, tol));
    }

    {
        T x = -0.61726470129338736021;
        T target = -0.54159215919020774693;
        CHECK(check(ReLi2(x), target, tol));
    }

    {
        T x = -9.67638114288432929042829389558;
        T target = 0.1737333358173242572150667176;
        CHECK(check(Cl2(x), target, tol));
    }

    {
        T x = 5.84984596945908213197227165279;
        T target = -0.796843752699907626682262925;
        CHECK(check(Cl2(x), target, tol));
    }
}

TEST_CASE("Double precision complex", "[C]") {
    using T = C;
    double tol = 14;

    {
        T x = {0.45711690978156322098, 0.098700002469498754569};
        T target = {0.51853389883316621039, 0.1314870167254059094};
        CHECK(check(Li2(x), target, tol));
    }

    {
        T x = {-5.0446694347946290631, -2.4172254883446985084};
        T target = {-2.8708212983887230642, -0.8408970915394507556};
        CHECK(check(Li2(x), target, tol));
    }

    {
        T x = {65.870251021681945452, -96.536885668539213637};
        T target = {-10.630039284312067744, -10.336498037579343249};
        CHECK(check(Li2(x), target, tol));
    }
}
