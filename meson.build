project(
  'Li2++',
  'cpp',
  version : '1.0.0',
  license : 'GPLv3',
  default_options : ['cpp_std=c++11', 'warning_level=3'],
  meson_version : '>=0.55'
)

if meson.get_compiler('cpp').get_id() == 'clang'
  add_project_arguments('-ferror-limit=2', language: 'cpp')
else
  add_project_arguments('-fmax-errors=2', language: 'cpp')
endif

deps = []
extra_cflags = []

conf_data = configuration_data()


# QD
if get_option('high-precision')
  deps += dependency('qd', required : true)
  conf_data.set('Li2pp_QD_ENABLED',true)
endif

# Allow linking to GMP
if get_option('gmp')
  warning('GMP is not fully supported yet!')
  meson.get_compiler('cpp').find_library(
    'gmpxx', required : true,
    has_headers: ['gmpxx.h','mpfr.h']
  )
  deps += dependency('mpfr', required: true)
  add_project_link_arguments('-lgmpxx', language: 'cpp')
  extra_cflags += ['-lgmpxx']
  conf_data.set('Li2pp_GMP_ENABLED',true)
  install_headers(['include/mpreal.h','include/gmp_r.h'], subdir : meson.project_name())
endif


configure_file(
  configuration : conf_data,
  output : 'Li2++config.h',
  install_dir : 'include/'+meson.project_name()
)

inc_dirs = include_directories(['.','src/','include/'])

subdir('src')
subdir('tests')

install_headers([
  'include/Li2++.h',
  'include/typedefs.h',
  ],
  subdir : meson.project_name()
)


pkg = import('pkgconfig')
pkg.generate(
   Li2pplib,
   description : 'Fast multi-precisoin numerical evaluation of dilogarithms',
   name : meson.project_name(),
   version : meson.project_version(),
   extra_cflags : extra_cflags,
   subdirs : meson.project_name(),
)

Li2pp_dep = declare_dependency(include_directories: inc_dirs, dependencies: deps, link_with: Li2pplib)
meson.override_dependency('Li2++', Li2pp_dep)
