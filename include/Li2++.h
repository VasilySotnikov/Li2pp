#pragma once

#include "Li2++config.h"

#include "typedefs.h"

#ifdef Li2pp_QD_ENABLED 
#include "qd/qd_real.h"
#endif // Li2pp_QD_ENABLED

#ifdef Li2pp_GMP_ENABLED 
#include "gmp_r.h"
#endif // Li2pp_GMP_ENABLED



namespace Li2pp {

R ReLi2(R);
C Li2(C);
R Cl2(R);

#ifdef Li2pp_QD_ENABLED
RHP ReLi2(RHP);
RHP Cl2(RHP);
CHP Li2(CHP);
RVHP ReLi2(RVHP);
RVHP Cl2(RVHP);
CVHP Li2(CVHP);
#endif

#ifdef Li2pp_GMP_ENABLED
RGMP ReLi2(const RGMP&);
RGMP Cl2(const RGMP&);
CGMP Li2(const CGMP&);
#endif


template <class T> inline T pi();

template <> inline R pi() { return M_PI; }
#ifdef Li2pp_QD_ENABLED
template <> inline RHP pi() { return dd_real::_pi; }
template <> inline RVHP pi() { return qd_real::_pi; }
template <> inline C pi() { return C(M_PI); }
template <> inline CHP pi() { return CHP(dd_real::_pi, 0.); }
template <> inline CVHP pi() { return CVHP(qd_real::_pi, 0.); }
#endif

#ifdef Li2pp_GMP_ENABLED
template <> inline RGMP pi() { return get_pi(); }
template <> inline std::complex<RGMP> pi() { return std::complex<RGMP>(get_pi(), RGMP(0)); }
#endif



} // namespace Li2pp
