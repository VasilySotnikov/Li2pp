#pragma once

#include "Li2++config.h"
#include <complex>

#if defined(Li2pp_QD_ENABLED)
struct dd_real;
struct qd_real;
#endif


namespace Li2pp {

#ifdef Li2pp_GMP_ENABLED 
class RGMP; 
typedef std::complex<RGMP> CGMP;
#endif // ifdef Li2pp_GMP_ENABLED


//! double precision real
typedef double R;
//! high precision real (2 x double)
#if defined(Li2pp_QD_ENABLED)
typedef dd_real RHP;
//! higher precision real (4 x double)
typedef qd_real RVHP;
#endif
//! double precision complex
typedef std::complex<R> C;
#if defined(Li2pp_QD_ENABLED)
//! high precision complex (2 x double)
typedef std::complex<RHP> CHP;
//! higher precision complex (4 x double)
typedef std::complex<RVHP> CVHP;
#endif

}
