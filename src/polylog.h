/**
 * @file polylog.h
 *
 * @date 19.11.2007 original source
 *
 * @author David A. Kosower and adaptations in BlackHat and Caravel
 *
 * @brief Dilogarithm and Clausen function
 *
 * Templated implementations are included in the file polylog.hpp, and explicit
 * overloading in polylog.cpp, polylog_HP.cpp and polylog_VHP.cpp
 *
 */
#pragma once

#include "Li2++.h"

#include <cassert>
#include <cmath>

#ifdef Li2pp_QD_ENABLED
#include "qd_suppl.h"
#endif

#ifdef Li2pp_GMP_ENABLED
#include "gmp_r.h"
#endif

namespace Li2pp {

template <class T> inline T sq(const T& x) { return (x * x); }
} // namespace Li2pp
