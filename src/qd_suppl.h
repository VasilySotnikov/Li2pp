#pragma once

#include "qd/qd_real.h"
#include <complex>
#include <functional>

namespace Li2pp {

class CPU_FIX {
    unsigned int old_cw;

  public:
    CPU_FIX() { fpu_fix_start(&old_cw); };
    ~CPU_FIX() { fpu_fix_end(&old_cw); };
};

std::complex<dd_real> pow(std::complex<dd_real> __x, int __n);
std::complex<qd_real> pow(std::complex<qd_real> __x, int __n);

std::complex<dd_real> sqrt(const std::complex<dd_real>& __z);
std::complex<qd_real> sqrt(const std::complex<qd_real>& __z);

inline dd_real abs(const std::complex<dd_real>& __z) {
    dd_real __x = __z.real();
    dd_real __y = __z.imag();
    const dd_real __s = std::max(abs(__x), abs(__y));
    if (__s == dd_real()) // well ...
        return __s;
    __x /= __s;
    __y /= __s;
    return __s * sqrt(__x * __x + __y * __y);
}

inline qd_real abs(const std::complex<qd_real>& __z) {
    qd_real __x = __z.real();
    qd_real __y = __z.imag();
    const qd_real __s = std::max(abs(__x), abs(__y));
    if (__s == qd_real()) // well ...
        return __s;
    __x /= __s;
    __y /= __s;
    return __s * sqrt(__x * __x + __y * __y);
}

inline dd_real arg(const std::complex<dd_real>& __z) { return atan2(__z.imag(), __z.real()); }
inline qd_real arg(const std::complex<qd_real>& __z) { return atan2(__z.imag(), __z.real()); }

inline std::complex<dd_real> polar(const dd_real& __rho, const dd_real& __theta) { return std::complex<dd_real>(__rho * cos(__theta), __rho * sin(__theta)); }

inline std::complex<qd_real> polar(const qd_real& __rho, const qd_real& __theta) { return std::complex<qd_real>(__rho * cos(__theta), __rho * sin(__theta)); }

inline std::complex<dd_real> exp(const std::complex<dd_real>& __z) { return polar(exp(__z.real()), __z.imag()); }

inline std::complex<qd_real> exp(const std::complex<qd_real>& __z) { return polar(exp(__z.real()), __z.imag()); }

inline std::complex<dd_real> log(const std::complex<dd_real>& __z) { return std::complex<dd_real>(log(std::abs(__z)), arg(__z)); }
inline std::complex<qd_real> log(const std::complex<qd_real>& __z) { return std::complex<qd_real>(log(std::abs(__z)), arg(__z)); }

inline std::complex<double> to_double(const std::complex<double>& c) { return c; }
inline std::complex<double> to_double(const std::complex<dd_real>& c) { return std::complex<double>(to_double(c.real()), to_double(c.imag())); }
inline std::complex<double> to_double(const std::complex<qd_real>& c) { return std::complex<double>(to_double(c.real()), to_double(c.imag())); }

inline std::complex<dd_real> to_HP(const std::complex<qd_real>& c) { return std::complex<dd_real>(to_dd_real(c.real()), to_dd_real(c.imag())); }
inline std::complex<dd_real> to_HP(const std::complex<dd_real>& c) { return c; }
inline std::complex<dd_real> to_HP(const std::complex<double>& c) { return std::complex<dd_real>(c.real(), c.imag()); }

inline std::complex<qd_real> to_VHP(const std::complex<qd_real>& c) { return c; }
inline std::complex<qd_real> to_VHP(const std::complex<dd_real>& c) { return std::complex<qd_real>(c.real(), c.imag()); }
inline std::complex<qd_real> to_VHP(const std::complex<double>& c) { return std::complex<qd_real>(c.real(), c.imag()); }

} // namespace Li2pp
