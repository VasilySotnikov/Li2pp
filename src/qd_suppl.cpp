#include "qd_suppl.h"
#include "typedefs.h"

namespace Li2pp {

std::complex<RHP> pow(std::complex<RHP> __x, int __n) {
    if (__n < 0) { return pow(CHP(1., 0.) / __x, -__n); }
    std::complex<RHP> __y;
    if (__n % 2) { __y = __x; }
    else {
        __y = CHP(1., 0.);
    }

    while (__n >>= 1) {
        __x = __x * __x;
        if (__n % 2) __y = __y * __x;
    }

    return __y;
}
std::complex<RVHP> pow(std::complex<RVHP> __x, int __n) {
    if (__n < 0) { return pow(CVHP(1., 0.) / __x, -__n); }
    std::complex<RVHP> __y;
    if (__n % 2) { __y = __x; }
    else {
        __y = CVHP(1., 0.);
    }

    while (__n >>= 1) {
        __x = __x * __x;
        if (__n % 2) __y = __y * __x;
    }

    return __y;
}

CHP sqrt(const CHP& __z) {
    RHP __x = __z.real();
    RHP __y = __z.imag();

    if (__x == RHP(0.)) {
        RHP __t = sqrt(abs(__y) / RHP(2.));
        return CHP(__t, __y < RHP(0.) ? -__t : __t);
    }
    else {
        RHP __t = sqrt(2 * (std::abs(__z) + abs(__x)));
        RHP __u = __t / RHP(2.);
        return __x > RHP(0.) ? CHP(__u, __y / __t) : CHP(abs(__y) / __t, __y < RHP(0.) ? -__u : __u);
    }
}

CVHP sqrt(const CVHP& __z) {
    RVHP __x = __z.real();
    RVHP __y = __z.imag();

    if (__x == RVHP(0.)) {
        RVHP __t = sqrt(abs(__y) / RVHP(2.));
        return CVHP(__t, __y < RVHP(0.) ? -__t : __t);
    }
    else {
        RVHP __t = sqrt(2 * (std::abs(__z) + abs(__x)));
        RVHP __u = __t / RVHP(2.);
        return __x > RVHP(0.) ? CVHP(__u, __y / __t) : CVHP(abs(__y) / __t, __y < RVHP(0.) ? -__u : __u);
    }
}

RHP sqrt(const RHP& __z);
RVHP sqrt(const RVHP& __z);

} // namespace Li2pp
